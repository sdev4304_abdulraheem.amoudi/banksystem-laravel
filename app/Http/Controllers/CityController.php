<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$cities = City::all();
        $cities = City::withCount('admins')->paginate(10);
        return response()->view('cms.cities.index',['cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('cms.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|min:3|max:30',
            'active' => 'in:on'
        ], [
            'name.required' => 'City name is required!',
            'name.min' => 'Name must be at least 3 characters'
        ]);
        $city = new City();
        $city->name = $request->get('name');
        $city->active = $request->has('active');
        $isSaved = $city->save();
        if($isSaved){
            session()->flash('message', 'City created successfully');
            return redirect()->route('cities.create');
        }else {

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $city = City::findorfail($id);
        return response()->view('cms.cities.edit', ['city' => $city]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|min:3|max:30',
            'active' => 'in:on'
        ]);
        
        $city = City::findOrFail($id);
        $city->name = $request->get('name');
        $city->active = $request->has('active');
        $isUpdated = $city->save();
        if($isUpdated){
            session()->flash('message', 'City Updated Successfully');
            session()->flash('alert-type', 'alert-success');
            session()->flash('check-circle', 'fa-check-circle');
            return redirect()->back();
        } else {
            session()->flash('message', 'Fail to updated City');
            session()->flash('alert-type', 'alert-danger');
            session()->flash('check-circle', 'fa-times');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $isDeleted = City::destroy($id);
        if ($isDeleted) {
            return response()->json(['title' => 'Deleted!', 'message' => 'City Deleted Successfully', 'icon' => 'success'],200);
        } else {
            return response()->json(['title' => 'failed!', 'message' => 'Delete city failed', 'icon' => 'error'],400);
        }
        
        
    }
}
