<?php

use App\Models\City;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string('first_name', 45);
            $table->string('last_name', 45);
            $table->string('email', 45)->unique();
            $table->string('mobile', 45)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            //$table->bigInteger(8)->unsigned()->autoIncrement()->primary();
            // $table->foreignId('city_id');

            $table->foreignIdFor(City::class);
            $table->foreign('City_id')->on('cities')->references('id');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
