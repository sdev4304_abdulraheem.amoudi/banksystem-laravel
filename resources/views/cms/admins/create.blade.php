@extends('cms.parent')

@section('title', 'CreateAdmin')


@section('page-name', 'Create Admin')
@section('main-page', 'Admins')
@section('sub-page', 'Create Admin')

@section('styles')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('cms/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('cms/plugins/toastr/toastr.min.css') }}">
@endsection

@section('content')

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Create Admin</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="create_form">
              @csrf
              <div class="card-body">
                <div class="form-group">
                  <label>Cities</label>
                  <select class="form-control select2" style="width: 100%;" id="city_id">
                    @foreach ($cities as $city)
                      <option value="{{ $city->id }}">{{ $city->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="first_name">First name</label>
                  <input type="text" class="form-control" id="first_name" placeholder="Enter first name">
                </div>
                <div class="form-group">
                  <label for="last_name">Last name</label>
                  <input type="text" class="form-control" id="last_name" placeholder="Enter last name">
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="mobile">Mobile</label>
                  <input type="text" class="form-control" id="mobile" placeholder="Enter mobile">
                </div>
                {{-- <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" name="active" id="active">
                    <label class="custom-control-label" for="active">Active Status</label>
                  </div>
                </div> --}}
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="button" onclick="store()" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->

@endsection

@section('scripts')
  <!-- Select2 -->
  <script src="{{ asset('cms/plugins/select2/js/select2.full.min.js') }}"></script>
  <script src="{{ asset('cms/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
  <!-- Toastr -->
  <script src="{{ asset('cms/plugins/toastr/toastr.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      bsCustomFileInput.init();
    });

  </script>

  <script>
    $(function() {
      //Initialize Select2 Elements
      $('.select2').select2({
        theme: 'bootstrap4'
      })
    })

  </script>

  <script>
    function store() {
      axios.post('/cms/admin/admins', {
          city_id: document.getElementById('city_id').value,
          first_name: document.getElementById('first_name').value,
          last_name: document.getElementById('last_name').value,
          email: document.getElementById('email').value,
          mobile: document.getElementById('mobile').value,
        })
        .then(function(response) {
          // handle success
          console.log(response.data);
          document.getElementById('create_form').reset();
          showToast(true, response.data.message);
        })
        .catch(function(error) {
          // handle error
          console.log(error.response);
          showToast(false, error.response.data.message);
        });
    };

    function showToast(status, message) {
      if (status) {
        toastr.success(message)
      } else {
        toastr.error(message)
      }
    }

  </script>

@endsection
