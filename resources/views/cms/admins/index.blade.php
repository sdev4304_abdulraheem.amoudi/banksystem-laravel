@extends('cms.parent')

@section('title', 'Admins')
@section('page-name', 'Index Admins')
@section('main-page', 'Admins')
@section('sub-page', 'Index')

@section('styles')
<link href="{{ asset('css/tailWind.css') }}" rel="stylesheet">
@endsection



@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Cities</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover table-bordered text-nowrap">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>F.Name</th>
                    <th>L.Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>City</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Settings</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($admins as $admin)
                    <tr>
                      <td>{{ $admin->id }}</td>
                      <td>{{ $admin->first_name }}</td>
                      <td>{{ $admin->last_name }}</td>
                      <td>{{ $admin->email }}</td>
                      <td>{{ $admin->mobile }}</td>
                      <td>{{ $admin->city->name }}</td>
                      {{-- <td><span @if ($city->active) class="badge bg-success" @else class="badge bg-danger" @endif>{{ $city->status }}</span></td> --}}
                      <td>{{ $admin->created_at->format('y-m-d') }}</td>
                      <td>{{ $admin->updated_at->format('y-m-d') }}</td>
                      <td>
                        <div class="btn-group">
                          <a href="{{ route('admins.edit', $admin->id) }}" class="btn btn-info"><i
                              class="fas fa-edit"></i></a>
                          <a href="#" class="btn btn-danger" onclick="confirmDestroy({{ $admin->id }}, this)"><i
                              class="fas fa-trash"></i></a>
                        </div>
                      </td>
                    </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
            <div class="card-footer clearfix">
              {{ $admins->links() }}
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.row -->
    </div>
  </section>
@endsection

@section('scripts')

  <script>
    function confirmDestroy(id, td) {
      console.log('CITY ID : ' + id);
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          destroy(id, td);
        }
      });
    }

    function destroy(id, td) {
      axios.delete('/cms/admin/cities/' + id)
        .then(function(response) {
          // handle success
          console.log(response);
          showAlert(response.data);
          td.closest('tr').remove();
        })
        .catch(function(error) {
          // handle error
          console.log(error);
          showAlert(error.response.data);
        })
        .then(function() {
          // always executed
        });
    }

    function showAlert(data) {
      let timerInterval
      Swal.fire({
        title: data.title,
        text: data.message,
        icon: data.icon,
        timer: 2000,
        showConfirmButton: false,
        timerProgressBar: false,
        didOpen: () => {
         // Swal.showLoading()
        },
        willClose: () => {
          clearInterval(timerInterval)
        }
      }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
          console.log('I was closed by the timer')
        }
      })
    }

  </script>

@endsection
