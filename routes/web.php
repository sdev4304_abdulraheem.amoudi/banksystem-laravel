<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CityController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('cms/admin')->group(function(){
    Route::view('/', 'cms.parent');
    Route::view('/dasboard', 'cms.dashboard')->name('cms.dashboard');
    Route::view('/fathers/index', 'cms.fathers.index')->name('cms.fathers.index');
    Route::view('/tables', 'cms.samples.simple');
    Route::view('/forms', 'cms.samples.general');
    Route::view('/advanced', 'cms.samples.advanced');
    Route::resource('cities', CityController::class);
    Route::resource('admins', AdminController::class);
});
